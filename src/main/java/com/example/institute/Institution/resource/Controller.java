package com.example.institute.Institution.resource;

import com.example.institute.Institution.models.Course;
import com.example.institute.Institution.models.Student;
import com.example.institute.Institution.services.InstituteService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.RestController;

import java.util.List;

@RestController
public class Controller {

    @Autowired
    InstituteService instituteService;

    @GetMapping("/")
    public List<Course> findallCourses(){
        return instituteService.findAllCourses();
    }

    @GetMapping("/name")
    public String findName(){
        return instituteService.studentName();
    }

    @GetMapping("/student")
    public List<Student> getStudents(){
        return instituteService.getStudents();
    }

    @GetMapping("/instituteId") // id is hardcoded in CourseRepo file
    public List<Course> getCourseByInstituteId(){
        return instituteService.getCourse();
    }
}
