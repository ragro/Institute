package com.example.institute.Institution;

import com.example.institute.Institution.models.Course;
import com.example.institute.Institution.models.CourseEnum;
import com.example.institute.Institution.models.Institute;
import com.example.institute.Institution.models.Student;
import com.example.institute.Institution.repositories.CourseRepo;
import com.example.institute.Institution.services.InstituteService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.CommandLineRunner;
import org.springframework.stereotype.Component;

import javax.security.auth.Subject;
import java.util.Arrays;
import java.util.HashSet;

@Component
public class seedDB implements CommandLineRunner {
    @Autowired
    InstituteService instituteService;

//    @Autowired
//    CourseRepo courseRepo;

    @Override
    public void run(String... args) throws Exception {

        Institute institute = new Institute("CODELIPI","Faridabad");
//        instituteService.addInstitute(institute);

        Student student = new Student("Nivi","14/cs63");
//        student.setInstitute(instituteService.getInstitute("CODELIPI"));
        student.setInstitute(institute);
//        instituteService.addStudent(student);
        Course course = new Course();
        course.setType(CourseEnum.ENGLISH);
        course.setInstitute(institute);

        course.setStudents(Arrays.asList(student));
        student.getCourses().add(course);

        institute.setStudentList(Arrays.asList(student));
        institute.getCourseList().add(course);
//        institute.setCourseList(new HashSet<Course>(course));
        instituteService.addInstitute(institute);

//        System.out.println(courseRepo.findCourseByInstituteId(1L));

        Institute institute1 = new Institute("DUCAT","Faridabad");

        Student student1 = new Student("Rohit", "14/cs75");
        student1.setInstitute(institute1);

        Course course1 = new Course();
        course1.setType(CourseEnum.SCIENCE);
        course1.setInstitute(institute1);
        course1.setStudents(Arrays.asList(student1));

        student1.getCourses().add(course1);

        institute1.getCourseList().add(course1);

        instituteService.addInstitute(institute1);

    }
}
