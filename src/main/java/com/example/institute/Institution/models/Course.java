package com.example.institute.Institution.models;

import javax.persistence.*;
import java.util.List;

@Entity
@Table(name = "Course")
public class Course {
    @Column(name = "courseId")
    @Id
    @GeneratedValue(strategy = GenerationType.AUTO)
    private Long courseId;

    @Column(name = "type")
    @Enumerated(EnumType.STRING)
    private CourseEnum type;


    @ManyToOne(fetch = FetchType.LAZY)
    @JoinColumn(name = "institute")
    private Institute institute;

//    @ManyToMany

    @Column(name = "studentId")
    @ManyToMany(mappedBy = "courses")
//    @JoinTable(name = "student_course" ,joinColumns = @JoinColumn(name = "course_id"), inverseJoinColumns = @JoinColumn(name = "student_id"))
    private List<Student> students;

    public Course() {
    }

    public Course(CourseEnum type, Institute institute) {
        this.type = type;
        this.institute = institute;
    }

    public Course(CourseEnum type, Institute institute, List<Student> students) {
        this.type = type;
        this.institute = institute;
        this.students = students;
    }

    public Long getCourseId() {
        return courseId;
    }

    public void setCourseId(Long courseId) {
        this.courseId = courseId;
    }

    public CourseEnum getType() {
        return type;
    }

    public void setType(CourseEnum type) {
        this.type = type;
    }

    public Institute getInstitute() {
        return institute;
    }

    public void setInstitute(Institute institute) {
        this.institute = institute;
    }

    public List<Student> getStudents() {
        return students;
    }

    public void setStudents(List<Student> students) {
        this.students = students;
    }

    @Override
    public String toString() {
        final StringBuilder sb = new StringBuilder("Course{");
        sb.append("courseId=").append(courseId);
        sb.append(", type='").append(type).append('\'');
        sb.append(", institute=").append(institute);
        sb.append(", students=").append(students);
        sb.append('}');
        return sb.toString();
    }
}
