package com.example.institute.Institution.models;

import javax.persistence.*;
import java.util.HashSet;
import java.util.Set;

@Entity
@Table(name = "Student")
public class Student {
    @Column(name="studentId")
    @Id
    @GeneratedValue(strategy = GenerationType.AUTO)

    private Long studentId;

    @Column(name = "name")
    private String name;

    @Column(name = "rollNo")
    private String rollNo;

//    @ManyToMany(mappedBy = "students")
    @Column(name = "course")
    @ManyToMany
    @JoinTable(name = "course_student" ,joinColumns = @JoinColumn(name = "course_id"), inverseJoinColumns = @JoinColumn(name = "student_id"))
    private Set<Course> courses = new HashSet<>();


//    @JoinTable(name="institute_student",joinColumns = @JoinColumn(name = "student_id"), inverseJoinColumns = @JoinColumn(name = "institute_id"))

    @ManyToOne(fetch = FetchType.LAZY)
    @JoinColumn(name="instituteId")
    private Institute institute;

    public Student() {
    }

    public Student(String name, String rollNo) {
        this.name = name;
        this.rollNo = rollNo;
    }

    public Student(Long studentId, String name, String rollNo, Set<Course> courses, Institute institute) {
        this.studentId = studentId;
        this.name = name;
        this.rollNo = rollNo;
        this.courses = courses;
        this.institute = institute;
    }

    public Long getStudentId() {
        return studentId;
    }

    public void setStudentId(Long studentId) {
        this.studentId = studentId;
    }

    public String getName() {
        return name;
    }

    public void setName(String name) {
        this.name = name;
    }

    public String getRollNo() {
        return rollNo;
    }

    public void setRollNo(String rollNo) {
        this.rollNo = rollNo;
    }

    public Set<Course> getCourses() {
        return courses;
    }

    public void setCourses(Set<Course> courses) {
        this.courses = courses;
    }

    public Institute getInstitute() {
        return institute;
    }

    public void setInstitute(Institute institute) {
        this.institute = institute;
    }

    @Override
    public String toString() {
        final StringBuilder sb = new StringBuilder("Student{");
        sb.append("studentId=").append(studentId);
        sb.append(", name='").append(name).append('\'');
        sb.append(", rollNo='").append(rollNo).append('\'');
        sb.append(", courses=").append(courses);
        sb.append(", institute=").append(institute);
        sb.append('}');
        return sb.toString();
    }
}
