package com.example.institute.Institution.models;

import javax.persistence.*;
import java.util.HashSet;
import java.util.List;
import java.util.Set;

//@Entity(name = "Institute")
//@Table(name = "institute")
//
//@NamedNativeQueries({
//        @NamedNativeQuery(
//                name = "getAllCourses",
//                query = "SELECT course_id, type"+
//                        "FROM course"+
//                        "WHERE institute_id = ?",
//                        resultClass = Institute.class
//        )
//})

@Entity
@Table(name = "Institute")
public class Institute {
    @Column(name = "instituteId")
    @Id
    @GeneratedValue(strategy = GenerationType.AUTO)
    private Long instituteId;

    @Column(name = "name")
    private String name;

    @Column(name = "city")
    private String city;

    @OneToMany(mappedBy = "institute", cascade = CascadeType.ALL)
    private List<Student> studentList;

    @OneToMany(mappedBy= "institute", cascade = CascadeType.ALL)
    private Set<Course> courseList = new HashSet<>();

    public Institute() {
    }

    public Institute(String name, String city) {
        this.name = name;
        this.city = city;
    }

    public Institute(String name, String city, List<Student> studentList, Set<Course> courseList) {
        this.name = name;
        this.city = city;
        this.studentList = studentList;
        this.courseList = courseList;
    }

    public Long getInstitute_id() {
        return instituteId;
    }

    public void setInstitute_id(Long institute_id) {
        this.instituteId = institute_id;
    }

    public String getName() {
        return name;
    }

    public void setName(String name) {
        this.name = name;
    }

    public String getCity() {
        return city;
    }

    public void setCity(String city) {
        this.city = city;
    }

    public List<Student> getStudentList() {
        return studentList;
    }

    public void setStudentList(List<Student> studentList) {
        this.studentList = studentList;
    }

    public Set<Course> getCourseList() {
        return courseList;
    }

    public void setCourseList(Set<Course> courseList) {
        this.courseList = courseList;
    }

    @Override
    public String toString() {
        final StringBuilder sb = new StringBuilder("Institute{");
        sb.append("institute_id=").append(instituteId);
        sb.append(", name='").append(name).append('\'');
        sb.append(", city='").append(city).append('\'');
        sb.append(", studentList=").append(studentList);
        sb.append(", courseList=").append(courseList);
        sb.append('}');
        return sb.toString();
    }
}
