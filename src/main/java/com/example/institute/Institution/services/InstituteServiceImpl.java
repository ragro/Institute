package com.example.institute.Institution.services;

import com.example.institute.Institution.models.Course;
import com.example.institute.Institution.models.Institute;
import com.example.institute.Institution.models.Student;
import com.example.institute.Institution.repositories.CourseRepo;
import com.example.institute.Institution.repositories.InstituteRepo;
import com.example.institute.Institution.repositories.StudentRepo;
import org.springframework.beans.factory.annotation.Autowired;

import java.util.List;

@org.springframework.stereotype.Service
public class InstituteServiceImpl implements InstituteService {

    @Autowired
    private StudentRepo studentRepo;
    @Autowired
    private InstituteRepo instituteRepo;
    @Autowired
    private CourseRepo courseRepo;

    @Override
    public void addStudent(Student student) {
        studentRepo.save(student);
    }

    @Override
    public void addCourse(Course course) {
        courseRepo.save(course);
    }

    @Override
    public void addInstitute(Institute institute) {
        instituteRepo.save(institute);
    }

    @Override
    public Institute getInstitute(String name) {
        return instituteRepo.findByName(name);
    }

    @Override
    public List<Course> findAllCourses() {
        return courseRepo.findALLCourse();
    }

    @Override
    public String studentName() {
        return studentRepo.getStudentName();
    }

    @Override
    public List<Student> getStudents() {
        return studentRepo.getStudents();
    }

    @Override
    public List<Course> getCourse() {
        return courseRepo.findCourses();
    }
}
