package com.example.institute.Institution.services;

import com.example.institute.Institution.models.Course;
import com.example.institute.Institution.models.Institute;
import com.example.institute.Institution.models.Student;

import java.util.List;

public interface InstituteService {
    public void addStudent(Student student);
    public void addCourse(Course course);
    public void addInstitute(Institute institute);
    public Institute getInstitute(String name);

    public List<Course> findAllCourses();
    public String studentName();
    public List<Student> getStudents();

    public List<Course> getCourse();
}