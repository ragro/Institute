package com.example.institute.Institution.repositories;

import com.example.institute.Institution.models.Student;
import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.data.jpa.repository.Query;
import org.springframework.stereotype.Repository;

import java.util.List;

@Repository
public interface StudentRepo extends JpaRepository<Student, Long> {
    @Query("SELECT s.studentId from Student s")
    public String getStudentName();

    @Query("SELECT s.studentId, s.name from Student s")
    public List<Student> getStudents();
}
