package com.example.institute.Institution.repositories;

import com.example.institute.Institution.models.Course;
import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.data.jpa.repository.Query;
import org.springframework.stereotype.Repository;

import java.util.List;

@Repository
public interface CourseRepo extends JpaRepository<Course, Long> {



    @Query("SELECT c.type, c.courseId from Course c ")
    public List<Course> findALLCourse();

    @Query("SELECT c.type, c.courseId from Course c WHERE c.institute.instituteId = 1")
    public List<Course> findCourses();

}
