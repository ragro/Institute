package com.example.institute.Institution.repositories;

import com.example.institute.Institution.models.Institute;
import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.stereotype.Repository;

import javax.persistence.EntityManager;
import javax.persistence.PersistenceContext;

@Repository
public interface InstituteRepo extends JpaRepository<Institute,Long> {

//    @PersistenceContext
//    private EntityManager manager;
    public Institute findByName(String name);

}
